\select@language {polish}
\select@language {polish}
\contentsline {section}{\numberline {1}Wst\IeC {\k e}p}{9}{section.1}
\contentsline {section}{\numberline {2}Procesory graficzne i zr\IeC {\'o}wnoleglanie oblicze\IeC {\'n}}{9}{section.2}
\contentsline {subsection}{\numberline {2.1}CUDA}{9}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Budowa GPU}{9}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Wskaz\IeC {\'o}wki implementacyjne dla j\IeC {\k e}zyka CUDA C}{11}{subsubsection.2.1.2}
\contentsline {subsection}{\numberline {2.2}OpenCL}{12}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Wskaz\IeC {\'o}wki implementacyjne dla j\IeC {\k e}zyka OpenCL C}{12}{subsubsection.2.2.1}
\contentsline {section}{\numberline {3}Informacje wprowadzaj\IeC {\k a}ce}{13}{section.3}
\contentsline {subsection}{\numberline {3.1}Rys historyczny}{13}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Arytmetyka modularna}{14}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Grupy cykliczne}{15}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}Cia\IeC {\l }a sko\IeC {\'n}czone}{15}{subsubsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.3}Pot\IeC {\k e}gowanie liczb ca\IeC {\l }kowitych modulo n}{15}{subsubsection.3.2.3}
\contentsline {subsubsection}{\numberline {3.2.4}DLP}{16}{subsubsection.3.2.4}
\contentsline {subsubsection}{\numberline {3.2.5}DHP}{16}{subsubsection.3.2.5}
\contentsline {subsection}{\numberline {3.3}Krzywe eliptyczne}{16}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}Informacje wst\IeC {\k e}pne}{16}{subsubsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.2}Arytmetyka punkt\IeC {\'o}w na krzywej eliptycznej}{17}{subsubsection.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.3}Znajdowanie punkt\IeC {\'o}w krzywej eliptycznej}{18}{subsubsection.3.3.3}
\contentsline {subsubsection}{\numberline {3.3.4}Logarytm dyskretny na krzywych eliptycznych - ECDLP}{19}{subsubsection.3.3.4}
\contentsline {subsection}{\numberline {3.4}Iloczyn Weila}{19}{subsection.3.4}
\contentsline {subsubsection}{\numberline {3.4.1}Obliczanie iloczynu Weila}{19}{subsubsection.3.4.1}
\contentsline {subsection}{\numberline {3.5}Odwzorowania biliniowe}{20}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}Biliniowy problem Diffiego Hellmana}{20}{subsection.3.6}
\contentsline {subsection}{\numberline {3.7}Protoko\IeC {\l }y i algorytmy wykorzystuj\IeC {\k a}ce odwzorowania biliniowe}{21}{subsection.3.7}
\contentsline {subsubsection}{\numberline {3.7.1}Protok\IeC {\'o}\IeC {\l } uzgadniania klucza Diffiego-Hellmana dw\IeC {\'o}ch stron}{21}{subsubsection.3.7.1}
\contentsline {subsubsection}{\numberline {3.7.2}Dwurundowy protok\IeC {\'o}\IeC {\l } uzgadniania klucza Diffiego-Hellmana trzech stron}{22}{subsubsection.3.7.2}
\contentsline {subsubsection}{\numberline {3.7.3}Jednorundowy protok\IeC {\'o}\IeC {\l } uzgadniania klucza Diffiego-Hellmana trzech stron}{22}{subsubsection.3.7.3}
\contentsline {subsection}{\numberline {3.8}Algorytmy biliniowe}{23}{subsection.3.8}
\contentsline {subsection}{\numberline {3.9}IBE - Identity Based Encryption}{23}{subsection.3.9}
\contentsline {subsubsection}{\numberline {3.9.1}Zmodyfikowany iloczyn Weila}{23}{subsubsection.3.9.1}
\contentsline {subsubsection}{\numberline {3.9.2}Generator $\mathit {G}_1$ parametru BDH}{24}{subsubsection.3.9.2}
\contentsline {subsubsection}{\numberline {3.9.3}MapToPoint}{24}{subsubsection.3.9.3}
\contentsline {subsubsection}{\numberline {3.9.4}System IBE}{24}{subsubsection.3.9.4}
\contentsline {section}{\numberline {4}Analiza mo\IeC {\.z}liwo\IeC {\'s}ci przy\IeC {\'s}pieszenia algorytm\IeC {\'o}w szyfrowych opartych na odwzorowaniach biliniowych}{25}{section.4}
\contentsline {section}{\numberline {5}Model systemu}{26}{section.5}
\contentsline {subsection}{\numberline {5.1}Schemat systemu kryptograficznego}{26}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Generowanie Parametr\IeC {\'o}w Publicznych i G\IeC {\l }\IeC {\'o}wnego Sekretu}{27}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Pobieranie wyci\IeC {\k a}gu}{28}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Szyfrowanie}{28}{subsection.5.4}
\contentsline {subsection}{\numberline {5.5}Deszyfrowanie}{29}{subsection.5.5}
\contentsline {section}{\numberline {6}Implementacja}{29}{section.6}
\contentsline {subsection}{\numberline {6.1}Klasa largeInt}{29}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Implementacja algorytmu Setup()}{30}{subsection.6.2}
\contentsline {subsubsection}{\numberline {6.2.1}Generowanie liczb pierwszych Solina i test pierwszo\IeC {\'s}ci Millera-Rabina}{31}{subsubsection.6.2.1}
\contentsline {section}{\numberline {7}Testy}{33}{section.7}
\contentsline {section}{\numberline {8}Wnioski}{33}{section.8}
\contentsline {section}{\numberline {9}S\IeC {\l }ownik}{33}{section.9}
\contentsline {section}{\numberline {10}Bibliografia}{33}{section.10}
